from flask import Flask
app = Flask(__name__)
import codeitsuisse.routes.square
import codeitsuisse.routes.tally_expense
import codeitsuisse.routes.imagesGPS
import codeitsuisse.routes.primesum
import codeitsuisse.routes.tetris
