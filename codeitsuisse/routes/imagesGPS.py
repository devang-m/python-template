import logging
import requests
import urllib.request

from flask import request, jsonify
from codeitsuisse import app
from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS

logger = logging.getLogger(__name__)

@app.route('/imagesGPS', methods=['POST'])
def get_lat_long():
    data = request.json
    ans = []
    for key in data:
        urllib.request.urlretrieve(key['path'], '00000001.jpg')
        img = Image.open('00000001.jpg')
        print(img)
        exif_data = {}
        info = img._getexif()
        if info:
            for tag, value in info.items():
                decoded_tag = TAGS.get(tag, tag)
                if decoded_tag == "GPSInfo":
                    gps_data = {}
                    for gps_tag in value:
                        sub_decoded_tag = GPSTAGS.get(gps_tag, gps_tag)
                        gps_data[sub_decoded_tag] = value[gps_tag]

                    exif_data[decoded_tag] = gps_data
                else:
                    exif_data[decoded_tag] = value

        ans.append(get_lat_lon(exif_data))
    return jsonify(ans)


def convert_to_degrees(value):
    deg_num, deg_denom = value[0]
    d = float(deg_num) / float(deg_denom)

    min_num, min_denom = value[1]
    m = float(min_num) / float(min_denom)

    sec_num, sec_denom = value[2]
    s = float(sec_num) / float(sec_denom)

    return d + (m / 60.0) + (s / 3600.0)


def get_lat_lon(exif_data):
    lat = None
    long = None
    dictionary = {}
    if "GPSInfo" in exif_data:
        gps_info = exif_data["GPSInfo"]

        gps_latitude = gps_info.get("GPSLatitude")
        gps_latitude_ref = gps_info.get('GPSLatitudeRef')
        gps_longitude = gps_info.get('GPSLongitude')
        gps_longitude_ref = gps_info.get('GPSLongitudeRef')

        if gps_latitude and gps_latitude_ref and gps_longitude and gps_longitude_ref:
            lat = convert_to_degrees(gps_latitude)
            if gps_latitude_ref == "S":
                lat *= -1

            long = convert_to_degrees(gps_longitude)
            if gps_longitude_ref == "W":
                long *= -1

    dictionary['lat'] = lat
    dictionary['long'] = long
    return dictionary
