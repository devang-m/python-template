import logging
import math
from flask import request, jsonify
from codeitsuisse import app


logger = logging.getLogger(__name__)
set = []
prime = []

# function to
# check prime number
def isPrime(x):
    # square root of x
    sqroot = int(math.sqrt(x))
    flag = True

    # since 1 is not
    # prime number
    if (x == 1):
        return False

    # if any factor is
    # found return false
    for i in range(2, sqroot + 1):
        if (x % i == 0):
            return False

    # no factor found
    return True


# function to evaluate
# all possible N primes
# whose sum equals S
def primeSum(total,S, index):
    global set, prime

    # if total equals S
    # And total is reached
    # using N primes
    if (total == S):
        # display the N primes
        return set
    if(total>sum):
        return


    # add prime[index]
    # to set vector
    set.append(prime[index])

    # include the (index)th
    # prime to total
    primeSum(total + prime[index], S, index + 1)

    # remove element
    # from set vector
    set.pop()

    # exclude (index)th prime
    return primeSum(total,S, index + 1)


def allPrime(S):
    global set, prime

    # all primes less
    # than S itself
    for i in range(1, S + 1):
        if (isPrime(i)):
            prime.append(i)
    return primeSum(0, S, 0)


@app.route('/primesum', methods=['POST'])
def evaluate1():
    x=[]
    input = int(request.json["input"])
    print input

    x=allPrime(input)
    if(x):
        logging.info("My result :{}".format(prime))
        return jsonify(prime)



