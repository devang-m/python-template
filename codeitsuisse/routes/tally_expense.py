import logging
import json

from flask import request, jsonify;
from codeitsuisse import app;

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

def tally_expense(people, expenses):
    balance = [[p, 0.0] for p in people]

    for exp in expenses:
        paid_by = exp['paidBy']
        exclude = exp.get('exclude', [])
        amount = float(exp['amount'])
        split_amount = amount / float(len(people) - len(exclude))

        for b in balance:
            if not b[0] in exclude:
                b[1] -= split_amount
            if b[0] == paid_by:
                b[1] += amount

    balance.sort(key=lambda n: n[1])
    transactions = []
    i = 0
    j = len(balance) - 1

    while i < j:
        if -balance[i][1] > balance[j][1]:
            value = balance[j][1] 
            balance[i][1] += value
            balance[j][1] -= value
            transactions.append({
                'from': balance[i][0],
                'to': balance[j][0],
                'amount': round(value, 2),
            })
            j -= 1
        else:
            value = -balance[i][1]
            balance[i][1] += value
            balance[j][1] -= value
            transactions.append({
                'from': balance[i][0],
                'to': balance[j][0],
                'amount': round(value, 2),
            })
            i += 1

    return transactions

@app.route('/tally-expense', methods=['POST'])
def tally_expense_app():
    app.logger.info('Tally Expense POST Input: ' + json.dumps(request.json))

    people = request.json['persons']
    expenses = request.json['expenses']

    output = {'transactions': tally_expense(people, expenses)}

    app.logger.info('Tally Expense POST Output:' + json.dumps(output))
    return jsonify(output)
