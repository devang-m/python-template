import logging
import json

from flask import request, jsonify;
from codeitsuisse import app;

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

def remove_completed_rows(matrix):
    

def add_block(matrix, block, column, rotation):
    if block == 'I':
        if rotation % 2 == 0:
            lower_bound = 0
            for i in range(20):
                if matrix[column][i]:
                    lower_bound += 1
            for i in range(lower_bound, lower_bound+4):
                matrix[column][i] = 1
        else:
            


def tetris(blocks):
    # convert string to list of chars
    blocks = list(blocks)
    actions = []
    matrix = [[0 for i in range(10)] for i in range(20)]



    return actions

@app.route('/tetris', methods=['POST'])
def tetris_app():
    app.logger.info('Tetris POST Input: ' + json.dumps(request.json))

    blocks = request.json['tetrominoSequence']
    output = {'actions': tetris(blocks)}

    app.logger.info('Tetris POST Output:' + json.dumps(output))
    return jsonify(output)
