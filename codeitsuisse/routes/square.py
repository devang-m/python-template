import logging

from flask import request, jsonify;

from codeitsuisse import app;

logger = logging.getLogger(__name__)

@app.route('/square', methods=['POST'])
def evaluate():
    if request.json and "input" in request.json:
        n = int(request.json['input'])
        ans = n*n
        return jsonify(ans)
